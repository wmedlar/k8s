CERTIFICATE := $(word 1,$(wildcard manifests/*sealed-secrets/cert.pem))
SECRETS := $(filter-out %sealed-secrets/secret.yaml,$(wildcard manifests/*/secret.yaml))
SEALEDSECRETS := $(patsubst %/secret.yaml,%/sealedsecret.yaml,$(SECRETS))

.DEFAULT: help
.PHONY: help
help:
	@echo "Usage: make [options] [target] ..."
	@echo "Targets:                          "
	@echo " ├─ apply                         "
	@echo " │   └─ apply updated manifests   "
	@echo " ├─ seal                          "
	@echo " │   └─ encrypt Secret manifests  "
	@echo " └─ help                          "
	@echo "     └─ show this message and exit"

.PHONY: apply
apply:
	git ls-files -co --exclude-standard manifests/ | sort | xargs -n 1 kubectl apply -f

.PHONY: seal
seal: $(CERTIFICATE) $(SEALEDSECRETS)

$(CERTIFICATE):
	kubeseal --controller-namespace kube-addons --fetch-cert > "$@"

%/sealedsecret.yaml: %/secret.yaml $(CERTIFICATE)
	cat "$<" | kubeseal --cert "$(CERTIFICATE)" --format yaml > "$@"
