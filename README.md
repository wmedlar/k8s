## Getting Started

### Ingress
Currently [Contour](https://github.com/heptio/contour) is used to wrap [Envoy](https://github.com/envoyproxy/envoy) as an ingress controller. Simplify DNS record accounting by assigning the controller's load balancer a static IP (rather than ephemeral -- the default):

```shell
$ gcloud compute addresses create ingress --region us-west1
$ gcloud compute addresses describe ingress --region us-west1 | grep ^address
address: 35.203.138.195
```

Update the `loadBalancerIP` value of the [Contour service](manifests/contour/service.yaml) to contain this static IP, then set up a DNS A record pointing `*.sakubot.info` to this IP.

## Tips

* (GCP) Give your own account full admin rights, rather than just the default `system:authenticated` role.
```shell
$ kubectl create clusterrolebinding cluster-admin-binding --clusterrole cluster-admin --user $(gcloud config get-value account)
```

* Use anti-affinity to minimize colocation of replicant pods on nodes, limiting downtime if running on unstable hardware (e.g., pre-emptible or spot instances).
```yaml
metadata:
  labels:
    app: my-app
spec:
  affinity:
    podAntiAffinity:
      preferredDuringSchedulingIgnoredDuringExecution:
        - weight: 100
          podAffinityTerm:
            topologyKey: kubernetes.io/hostname
            labelSelector:
              matchLabels:
                app: my-app
```
